const menu = document.querySelector(".mobile-menu");
const navMenu = document.querySelector(".header__list");
const navLink = document.querySelectorAll(".header__link");

const mobileMenu = () => {
    menu.classList.toggle("active");
    navMenu.classList.toggle("active");
}

const closeMenu = () => {
    menu.classList.remove("active");
    navMenu.classList.remove("active");
}

menu.addEventListener("click", mobileMenu);
navLink.forEach(n => n.addEventListener("click", closeMenu));
